package web;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;
import junit.framework.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Login {

    static FirefoxDriver driver;
    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String username;
    private static String password;
    private static String base_url;
    private static String token;
    private static String notValidPass;
    private static String notValidUser1;
    private static String notValidUser2;
    private  static Long time = 5l;

    @BeforeEach
    void init() throws IOException {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--incognito");
        options.addArguments("start-maximized");
        driver = new FirefoxDriver(options);
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
        driver.get("https://test-stand.gb.ru/login");

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        base_url = prop.getProperty("base_url");
        token = prop.getProperty("token");
        notValidPass = prop.getProperty("notValidPass");
        notValidUser1 = prop.getProperty("notValidUser1");
        notValidUser2 = prop.getProperty("notValidUser2");
    }
    public static String getUsername() {
        return username;
    }

    public static String getPassword(){
        return password;
    }

    public static String getBase_url(){
        return base_url;
    }
    public static String getToken(){
        return token;
    }
    public static String getNotValidPass(){
        return notValidPass;
    }
    public static String getNotValidUser1(){
        return notValidUser1;
    }
    public static String getNotValidUser2(){
        return notValidUser2;
    }

    @AfterEach
    void close(){
        driver.quit();
    }
// Успешный вход, открывается страница с постами
@Test
    void login(){
    WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
    userName.sendKeys(getUsername());

    WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
    password.sendKeys(getPassword());

    WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
    button.click();


    Assertions.assertNotNull(driver.findElement(By.cssSelector(".content")));

}

// Неверный логин
@Test
    void notValidUsername(){
    WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
    userName.sendKeys(getNotValidUser2());

    WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
    password.sendKeys(getPassword());

    WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
    button.click();

    Assertions.assertNotNull(driver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
}

// Короткий логин
@Test
    void name2Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys(getNotValidUser1());

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys(getPassword());

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }

// Логин из 3 символов
    @Test
    void name3Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("Art");

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys("978d7d6bde");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.cssSelector(".content")));
    }

    // Логин из 4 символов
    @Test
    void name4Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("Arte");

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys("e2e14219d8");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.cssSelector(".content")));
    }

    // Логин из 19 символов
    @Test
    void name19Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("qwertyuiopasdfghjkl");

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys("4635f5995c");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.cssSelector(".content")));
    }

    // Логмн из 20 символов
    @Test
    void name20Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("ytrewquiopasdfghjklz");

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys("aa374350c4");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.cssSelector(".content")));
    }

    // Логин из 21 символов
    @Test
    void name21Latter(){
        WebElement userName = driver.findElement(By.xpath("//input[@type='text']"));
        userName.sendKeys("ytrewquiopasdfghjklzs");

        WebElement password = driver.findElement(By.xpath("//input[@type='password']"));
        password.sendKeys("aa374350c4");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        button.click();

        Assertions.assertNotNull(driver.findElement(By.xpath("//p[contains(text(),'Invalid credentials')]")));
    }




}
